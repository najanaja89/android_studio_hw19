package com.example.hw19;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

import java.security.KeyStore;
import java.util.List;
import java.util.Locale;


@SuppressLint("MissingPermission")
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityCompat.requestPermissions(
                MainActivity.this,
                new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                },
                100
        );

        LocationManager locationManager =  (LocationManager) getSystemService(LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);
        Location locations = locationManager.getLastKnownLocation(provider);
        List<String> providerList = locationManager.getAllProviders();
        if(locations !=null && providerList != null && providerList.size() > 0){
            double lat = locations.getLatitude();
            double lon = locations.getLongitude();
            Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
                if(addresses !=null && addresses.size()>0){
                    Log.d("Hello", addresses.get(0).getAddressLine(0));
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                .getMapAsync(new OnMapReadyCallback() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        googleMap.setMyLocationEnabled(true);
                        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(51.1283, 71.4303))
                                .zoom(13f)
                                .bearing(90)
                                .tilt(45)
                                .build();

                        CameraUpdate cameraUpdate = CameraUpdateFactory
                                .newCameraPosition(cameraPosition);
                        googleMap.animateCamera(cameraUpdate );



                        PolygonOptions polygonOptions = new PolygonOptions();

                        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                            @Override
                            public void onMapClick(LatLng latLng) {
                                BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory
                                        .fromResource(R.drawable.location);
                                MarkerOptions markerOptions = new MarkerOptions();
                                markerOptions.position(latLng);
                                markerOptions.icon(bitmapDescriptor);

                                googleMap.addMarker(markerOptions);
                            }
                        });
                        Location location1 = new Location("");
                        location1.setLatitude(47.1228);
                        location1.setLongitude(51.9609);

                        Location location2 = new Location("");
                        location2.setLatitude(47.1229);
                        location2.setLongitude(51.9610);

                        Log.d("Hello", "Distance " + location1.distanceTo(location2));
                    }
                });

    }
}